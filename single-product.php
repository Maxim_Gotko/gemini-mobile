<?php
/**
 * The template for displaying all single posts and attachments
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */

get_header(); ?>
<?php
// Start the loop.
while (have_posts()) : the_post();
$product_id = get_the_ID();
?>

<div class="header-product">
    <div class="container">
            <a href="javascript:history.go(-1); " class="back-link">
            <?php the_title(); ?>
        </a>

    </div>
    <!-- /.container -->
</div>
<!-- /.header-subcategory -->
<div class="product-section">
    <div class="container">
        <div class="product-name-wrap">
            <div class="main-product-image">
                <div class="product-image-wrap">
                    <?php the_post_thumbnail(); ?>
                </div>
                <!-- /.product-image-wrap -->
            </div>
            <!-- /.main-product-image -->
            <div class="product-name">
                <h1>
                    <?php the_title(); ?>
                </h1>
                <a href="<?php the_field('href_online_shop', 'option') ?>" class="btn">
                    <?php echo __('In shop','gemini');?>
                </a>
            </div>
            <!-- /.product-name -->
        </div>
        <!-- /.product-name-wrap -->
        <div class="product-info-block">
          <?php the_content(); ?>
        </div>
        <!-- /.product-info -->
    </div>
    <!-- /.container -->
</div>
<!-- /.product-info-section -->


<?php endwhile; ?>
<!-- /.callback-section -->
<?php get_footer(); ?>
