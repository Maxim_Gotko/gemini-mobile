'use strict';

var gulp = require('gulp'),
  prefixer = require('gulp-autoprefixer'),
  cssmin = require('gulp-clean-css'),
  imagemin = require('gulp-imagemin'),
  rigger= require('gulp-rigger'),
  pngquant = require('imagemin-pngquant'),
  sass = require('gulp-sass'),
  uglify = require('gulp-uglify'),
  watch = require('gulp-watch'),
  sourcemaps = require('gulp-sourcemaps'),
  browserSync = require("browser-sync"),
    livereload = require('gulp-livereload'),

  reload = browserSync.reload;

var path = {
  build: {
    js: './js/',
    css: './css/',
    img: './img/',
    fonts: './fonts/',
    libs: './libs/'
  },
  src: {
    js: 'src/js/main.js',
    style: 'src/style/style.scss',
    img: 'src/img/**/*.*',
    fonts: 'src/fonts/**/*.*',
    libs: 'src/libs/**/*.*'
  },
  watch: {
    js: 'src/js/**/*.js',
    style: 'src/style/**/*.scss',
    img: 'src/img/**/*.*',
    fonts: 'src/fonts/**/*.*'
  }
};

var config = {
  server: {
    baseDir: "../"
  },
  tunnel: true,
  host: 'gemini-mobile',
  port: 9000,
  logPrefix: "Frontend_Develop"
};

gulp.task('html:build', function () {
  return gulp.src(path.src.html)
    .pipe(rigger())
    .pipe(gulp.dest(path.build.html))
    .pipe(reload({stream: true}));
});

gulp.task('js:build', function () {
  return gulp.src(path.src.js)
    .pipe(rigger())
    .pipe(sourcemaps.init())
    .pipe(uglify())
    .pipe(sourcemaps.write())
    .pipe(gulp.dest(path.build.js))
    .pipe(reload({stream: true}));
});

gulp.task('style:build', function () {
  return  gulp.src(path.src.style)
    .pipe(sourcemaps.init())
    .pipe(sass().on('error', sass.logError))
    .pipe(prefixer())
    .pipe(cssmin())
    .pipe(sourcemaps.write())
    .pipe(gulp.dest(path.build.css))
      .pipe(livereload())
    .pipe(reload({stream: true}));
});

gulp.task('image:build', function () {
  return  gulp.src(path.src.img)
    // .pipe(imagemin({
    //   progressive: true,
    //   svgoPlugins: [{removeViewBox: false}],
    //   use: [pngquant()],
    //   interlaced: true
    // }))
    .pipe(gulp.dest(path.build.img))
    .pipe(reload({stream: true}));
});

gulp.task('fonts:build', function() {
  return  gulp.src(path.src.fonts)
    .pipe(gulp.dest(path.build.fonts))
});

gulp.task('libs:build',function () {
   return gulp.src(path.src.libs)
       .pipe(gulp.dest(path.build.libs));
});

gulp.task('build', [
  'js:build',
  'style:build',
  'fonts:build',
  'libs:build',
  'image:build'
]);

gulp.task('watch', function(){
    livereload.listen();
  watch([path.watch.style], function(event, cb) {
    gulp.start('style:build');
  });
  watch([path.watch.js], function(event, cb) {
    gulp.start('js:build');
  });
  watch([path.watch.img], function(event, cb) {
    gulp.start('image:build');
  });
  watch([path.watch.fonts], function(event, cb) {
    gulp.start('fonts:build');
  });
});

gulp.task('webserver', function () {
  browserSync(config);
});

gulp.task('clean', function (cb) {
  rimraf(path.clean, cb);
});

gulp.task('default', ['build', 'webserver', 'watch']);