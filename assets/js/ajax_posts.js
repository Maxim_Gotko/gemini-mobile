$(document).ready(function () {
    var page = 2;
    
    $('.show-more').on('click', function (e) {
        e.preventDefault();
        var data = {
            action: 'load_more_posts_mobile',
            page: page,
            lan_id: $('body').attr('data-current-language'),
            security: myajax.nonce,
            beforeSend: function () {
                var $preloader = $('.preloader'),
                $spinner   = $preloader.find('.preloader-wrap');
                $spinner.fadeIn();
                $preloader.fadeIn();
                $preloader.delay(350).fadeOut('slow');
            },
        };
        jQuery.post(myajax.url, data, function (response) {
            if(response == 'last page'){
                $('.show-more').remove();
            }else{
                $('body .news-list').append(response)
                page++;
            }
        });

    });
    

});