<?php
/**
 * Template Name: service
 *
 */

get_header(); $page_id = get_the_ID(); ?>

<div class="header-tasting">
    <div class="parallax-scene" id="scene">
        <div class="header-bg-layer-1" data-depth="0.2"></div>
        <!-- /.header-bg-layer-1 -->
        <div class="header-bg-layer-2" data-depth="0.1"></div>
        <!-- /.header-bg-layer-2 -->
        <div class="header-bg-layer-3" data-depth="0.3"></div>
        <!-- /.header-bg-layer-2 -->
    </div>
    <div class="container">
        <h1><?php the_title(); ?></h1>
        <a href="#" onclick="history.back();" class="left-arrow">
            <?php echo __('Back','gemini'); ?>
        </a>
    </div>
    <!-- /.container -->
</div>
<!-- /.news-category -->
<div class="container">
    <?php while (have_posts()) : the_post(); ?>
    <div class="tasting-info">
        <?php the_content(); ?>
    </div>
    <?php endwhile; ?>
    <!-- /.html -->
</div>
<!-- /.container -->
<div class="services-section">
    <div class="container">
        <div class="statistics-block">

            <?php while (have_rows('services', $page_id)) : the_row(); ?>

            <a href="<?php the_sub_field('href'); ?>" class="statistics-item ">
                <div class="statistics-image-wrap statistics-year">
                    <?php $image_src = get_sub_field('img'); ?>
                    <img src="<?php echo $image_src; ?>" srcset="<?php echo $image_src; ?> , <?php echo get_srcset_by_img_src($image_src); ?>" alt="image">                </div>
                <!-- /.advantage-image-wrap -->
                <strong>
                    <?php the_sub_field('title'); ?>
                    <span class="shadow_effect">
                        <?php the_sub_field('title'); ?>
                    </span>
                    <!-- /.shadow_effect -->
                </strong>
                <p><?php the_sub_field('desc'); ?></p>

            </a>
            <?php endwhile; ?>
        </div>
    </div>
    <!-- /.container -->
</div>
<!-- /.services-section -->



<!-- /.callback-section -->

<?php get_template_part('template-parts/callback-section'); ?>

<!-- /.callback-section -->
<?php get_footer(); ?>
