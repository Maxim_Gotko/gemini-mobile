<?php
/**
 * Template Name: home
 *
 */

get_header();
?>
<?php $page_id = get_the_ID(); ?>
<div class="header-section">

    <div class="container">
        <div class="header-section-wrap">
            <?php while ( have_rows('banner_block', $page_id) ) : the_row(); ?>
                <div class="header-title">
                    <?php the_sub_field('top_text_1'); ?>
                </div>
                <!-- /.header-title -->

            <?php endwhile; ?>
        </div>
        <!-- /.header-section-wrap -->

    </div>
    <!-- /.container -->
</div>
<!-- /.header-section -->


<!-- /.advantage_section -->
<div class="shop-section">
    <div class="container">
        <h2 class="section-title">
            <?php echo __('Production','gemini-mobile'); ?>
        </h2>
        <div class="shop-block home-page-slider">
            <div class="swiper-wrapper">


            <?php
            /*
             * Get all product_category
             */
            $terms = get_terms( array(
                'taxonomy' => 'product-category',
                'parent' => 0,
                'orderby' => 'meta_value',
                'meta_key' => 'order',
                'exclude'       => array('64', '70', '66', '67', '92', '79', '163')
            ) );





            ?>
            <?php foreach($terms as $term): ?>
                <a href="<?php echo get_term_link($term->term_id); ?>" class="shop-item swiper-slide">
                    <?php $image_src = get_field('cat_img', $term ); ?>
                    <img src="<?php echo $image_src; ?>" srcset="<?php echo $image_src; ?> , <?php echo get_srcset_by_img_src($image_src); ?>" alt="image">
                    <strong>
                        <?php echo $term->name; ?>
                    </strong>
                    <p>
                        <?php  echo $term->description; ?>
                    </p>
                </a>
            <?php endforeach; ?>
            <!-- /.shop-item -->

            <!-- /.shop-item -->
            <a href="<?php the_field('href_online_shop','option'); ?>" class="shop-link swiper-slide">
                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/shop_img.png"
                     srcset="<?php echo get_template_directory_uri(); ?>/assets/img/shop_img.png 1x, <?php echo get_template_directory_uri(); ?>/assets/img/shop_img@2x.png 2x, <?php echo get_template_directory_uri(); ?>/assets/img/shop_img@3x.png 3x" alt="image">
                <strong>
                    <?php the_field('online_shop', 'option'); ?>
                </strong>
            </a>
            <!-- /.shop-item -->
            </div>
            <!-- /.swiper-wrapper -->
        </div>
        <!-- /.shop-block -->
    </div>
    <!-- /.container -->
</div>
<!-- /.shop-section -->

<div class="coffe-machine">
    <div class="container">
        <h2 class="section-title">
            <?php echo __('Coffee-machines','gemini-mobile'); ?>
        </h2>
        <div class="shop-block home-page-slider">
            <div class="swiper-wrapper">

                <?php

                $terms = get_terms( array(
                    'taxonomy' => 'product-category',
                    'parent' => 0,
                    'orderby' => 'meta_value',
                    'meta_key' => 'order',
                    'exclude'       => array('37', '47', '27', '6', '56', '7')
                ) );

    
                    foreach ( $terms  as $term ) {
                    if ( $term->parent=='0' ) {
                ?>
                
                        <div class="machine-item swiper-slide">
                           <a href="<?php echo get_term_link($term->term_id ); ?>"> <img  src="<?php echo get_field('cat_img', 'product-category_'.  $term->term_id ) ?>"></a>
                            <p><?php echo $term->name ?></p>
                        </div>

                <?php
                 }
                    }
                ?>

            </div>
            <!-- /.swiper-wrapper -->

        </div>
        <!-- /. -->
    </div>
    <!-- /.container -->
</div>
<!-- /.coffe-machine -->

<div class="service-section">
    <div class="container">
        <h2 class="section-title">
            <?php echo __('Services','gemini-mobile'); ?>
        </h2>
        <div class="services-block home-page-slider">
            <div class="swiper-wrapper">


            <?php

           

                $args = array(
                    'posts_per_page' => -1,
                    'post_type' => 'gemini_service',
                    'orderby' => 'meta_value',
                    'meta_key' => 'order'
                );

                $query = new WP_Query( $args );

                if ( $query->have_posts() ) {
                    while ( $query->have_posts() ) {
                        $query->the_post();
            ?>
       
                <div class="service-item swiper-slide">
                    <div class="img-wrap">
                        <a href="<?php echo get_permalink(); ?>"><img  src="<?php echo get_the_post_thumbnail_url(); ?>"></a>
                    </div>
                    <!-- /.imp-wrap -->

                    <strong><?php the_title(); ?></strong>
                    <p>
                        <?php 
                             $content = get_the_content();

                            $service_content =  wp_trim_words( $content , 20, '...' );

                            echo mb_strimwidth($service_content, 0, 82, '...');


                        ?>
                        <!--   <a class="service_mobile_more" href="<?php the_permalink(); ?>"><?php echo __('MORE','gemini'); ?>

                            </a> -->
                    </p>
                </div>

                
                
                <?php      
                 } // endwhile
            } else {

            }
            wp_reset_postdata(); ?>

            </div>
            <!-- /.swiper-wrapper -->

        </div>
        <!-- /. -->
    </div>
    <!-- /.container -->
</div>
<!-- /.coffe-machine -->

<div class="news-section">
    <div class="container">
        <h2 class="section-title"><?php the_field('title_block_news', $page_id); ?></h2>

        <div class="news-block home-page-slider">
            <div class="swiper-wrapper">


            <?php
            $args = array(
                'post_type' => 'post',
                'order' => 'DESC',
                'posts_per_page' => 4
            );
            $the_query = new WP_Query($args);
            while ( $the_query->have_posts()) :
                $the_query->the_post(); ?>
                <div class="news-item swiper-slide">
                    <a href="<?php the_permalink(); ?>" class="news-img-wrap">
                    <?php the_post_thumbnail(); ?>
                    </a>
                    <!-- /.news-img-wrap -->
                    <div class="news-text-wrap">

                    <?php 
                        $title = get_the_title();
                         $news_title =  wp_trim_words( $title , 20, '...' );

                    ?>
                        <h3><?php  echo mb_strimwidth($news_title, 0, 30, '...'); ?></h3>
                    <span class="public-date">
                        <?php echo get_the_date(); ?>
                    </span>
                        <!-- /.public-date -->
                        <p>
                            <?php
                            $content = get_the_content();
                            $news_content =  wp_trim_words( $content , 20, '...' );
                            echo mb_strimwidth($news_content, 0, 82, '...');
                            // if(strlen($content) > 4):
                            //     echo trim(substr($content, 0, 5)).'...';
                            // else:
                            //     echo $content;
                            // endif;
                            ?>
                            <?php global $sitepress;
                            $current_language = $sitepress->get_current_language();?>
                            <a href="<?php the_permalink(); ?>"><?php echo __('MORE','gemini'); ?>

                            </a>
                        </p>
                    </div>
                    <!-- /.news-text-wrap -->
                </div>
            <?php endwhile; wp_reset_postdata(); ?>
            </div>
            <!-- /.swiper-wrapper -->
        </div>
        <!-- /.news-container -->
    </div>
    <!-- /.container -->
</div>
<!-- /.news-section -->


<?php get_footer(); ?>
