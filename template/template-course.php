<?php
/**
 * Template Name: course
 *
 */

get_header(); $page_id = get_the_ID(); ?>

<div class="header-tasting">
    <div class="container">
        <h1><?php the_title(); ?></h1>
        <a href="javascript:history.go(-1); " class="left-arrow">
            <?php echo __('Back','gemini'); ?>
        </a>
    </div>
    <!-- /.container -->
</div>

<!-- /.course-section -->
<div class="course-section">
    <div class="container">
        <div class="tasting-info">
            <?php the_content(); ?>
        </div>
    </div>
    <!-- /.container -->
</div>
<!-- /.course-section -->


<?php get_footer(); ?>
