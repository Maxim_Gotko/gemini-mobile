<?php
/**
 * Template Name: services
 *
 */

get_header(); $page_id = get_the_ID(); ?>

<div class="header-services">
    <div class="container">
        <h1><?php the_title(); ?></h1>
        <a href="javascript:history.go(-1); " class="left-arrow">
            <?php echo __('Back','gemini'); ?>
        </a>
    </div>
    <!-- /.container -->
</div>
<!-- /.news-category -->
<div class="services-section">
    <div class="container">
        <div class="statistics-block">

            <?php

            $args = array(
                'posts_per_page' => -1,
                'orderby' => 'date',
                'post_type' => 'gemini_service'
            );

            $query = new WP_Query( $args );

            if ( $query->have_posts() ) {
                while ( $query->have_posts() ) {
                    $query->the_post();
                    ?>
                    <a href="<?php echo get_permalink(); ?>" class="statistics-item ">
                        <div class="statistics-image-wrap statistics-year">
                            <img src="<?php echo get_the_post_thumbnail_url(); ?>" srcset="<?php echo get_the_post_thumbnail_url(); ?> , <?php echo get_srcset_by_img_src(get_the_post_thumbnail_url()); ?>" alt="image">                </div>
                        <!-- /.advantage-image-wrap -->
                        <strong>
                            <?php echo get_the_title(); ?>
                            <span class="shadow_effect">
                        <?php echo get_the_title(); ?>
                    </span>
                            <!-- /.shadow_effect -->
                        </strong>
                        <p><?php echo the_excerpt(); ?></p>

                    </a>
                    <?php
                }
            } else {

            }
            wp_reset_postdata();

            ?>
        </div>
    </div>
    <!-- /.container -->
</div>
<!-- /.services-section -->


<div class="news-section">
    <div class="container">
        <h2><?php echo __('News','gemini'); ?> </h2>

        <div class="news-block">
            <?php
            $args = array(
                'order' => 'DESC',
                'posts_per_page' => 4
            );
            $the_query = new WP_Query($args);
            while ( $the_query->have_posts()) :
                $the_query->the_post(); ?>
                <div class="news-item">
                    <a href="<?php the_permalink(); ?>" class="news-img-wrap">
                        <?php the_post_thumbnail(); ?>
                    </a>
                    <!-- /.news-img-wrap -->
                    <div class="news-text-wrap">
                        <h3><?php the_title(); ?></h3>
                        <span class="public-date">
                        <?php echo get_the_date(); ?>
                    </span>
                        <!-- /.public-date -->
                        <p>
                            <?php
                            $content = get_the_content();
                            echo wp_trim_words( $content , 40, '...' );
                            ?>
                            <?php global $sitepress;
                            $current_language = $sitepress->get_current_language();?>
                            <a href="<?php the_permalink(); ?>"><?php echo __('MORE','gemini'); ?>
                            </a>
                        </p>
                    </div>
                    <!-- /.news-text-wrap -->
                </div>
            <?php endwhile; wp_reset_postdata(); ?>
        </div>
        <!-- /.news-container -->
    </div>
    <!-- /.container -->
</div>
<!-- /.news-section -->

<!-- /.callback-section -->

<?php get_template_part('template-parts/callback-section'); ?>

<!-- /.callback-section -->
<?php get_footer(); ?>
