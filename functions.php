<?php
header('Access-Control-Allow-Origin: *'); // for any host
// header('Access-Control-Allow-Origin: myhost.com');
//функция подключения скриптов и стилей

function gemini_scripts()
{

    wp_enqueue_script('paralax_js', get_template_directory_uri() . '/assets/libs/parallax.js', array(), '20141023', false);
    wp_enqueue_script('jquery_custom', get_template_directory_uri() . '/assets/libs/jquery.min.js', array(), '20141023');
    wp_enqueue_script('jquery_min_custom', get_template_directory_uri() . '/assets/libs/swiper.jquery.min.js', array(), '20141023', true);
    wp_enqueue_script('main_js', get_template_directory_uri() . '/assets/js/main.js', array(), '20141024', true);
    wp_enqueue_script('form_js', get_template_directory_uri() . '/assets/js/send_form.js', array(), '20141023', true);
    wp_enqueue_script('search_js', get_template_directory_uri() . '/assets/js/search.js', array(), '20141023', true);
    wp_enqueue_script('ajax_posts', get_template_directory_uri() . '/assets/js/ajax_posts.js', array(), '201410423', true);
    if ( is_single() ) {
        wp_enqueue_script('for_single', get_template_directory_uri() . '/assets/js/for_single.js', array(), '20141023', true);
    }
    wp_enqueue_style('main_css', get_template_directory_uri() . '/assets/css/style.css', array(), true);

}

add_action('wp_enqueue_scripts', 'gemini_scripts');


add_theme_support('post-thumbnails'); //включаем возможность задавать миниатюры


register_nav_menus(array(
    'main_menu' => __('Главное меню') //регистрируем меню для Хедера
));

if (function_exists('acf_add_options_page')) {

    acf_add_options_page('Лого\Соц сети\Copyright'); //страница опций для ДОП. настроек
    acf_add_options_page('Блок с картой'); //страница опций для ДОП. настроек
    acf_add_options_page('Блок Преимуществ'); //страница опций для ДОП. настроек
    acf_add_options_page('Ссылка Онлайн Магазин'); //страница опций для ДОП. настроек
}

show_admin_bar(false); // скрываем админ бар

/*
 * Регистрируем новый тип записи - Продукты
*/
add_action('init', 'create_taxonomy');
function create_taxonomy()
{
    // заголовки
    $labels = array(
        'name' => 'Категория',
        'singular_name' => 'Категория',
        'search_items' => 'Поиск Категория',
        'all_items' => 'Все Категории',
        'parent_item' => 'Родительская Категория',
        'parent_item_colon' => 'Родительская Категория:',
        'edit_item' => 'Добавить Категорию',
        'update_item' => 'Обновить Категорию',
        'add_new_item' => 'Добавить новую Категорию',
        'new_item_name' => 'Новая Категория',
        'menu_name' => 'Категории',
    );
    // параметры
    $args = array(
        'label' => '', // определяется параметром $labels->name
        'labels' => $labels,
        'slug' => 'taxonomy',
        'public' => true,
        'publicly_queryable' => null, // равен аргументу public
        'show_in_nav_menus' => true, // равен аргументу public
        'show_ui' => true, // равен аргументу public
        'show_tagcloud' => true, // равен аргументу show_ui
        'hierarchical' => true,
        'update_count_callback' => '',
        'rewrite' => true,
        //'query_var'             => $taxonomy, // название параметра запроса
        'capabilities' => array(),
        'meta_box_cb' => null, // callback функция. Отвечает за html код метабокса (с версии 3.8): post_categories_meta_box или post_tags_meta_box. Если указать false, то метабокс будет отключен вообще
        'show_admin_column' => true, // Позволить или нет авто-создание колонки таксономии в таблице ассоциированного типа записи. (с версии 3.5)
        '_builtin' => false,
        'show_in_quick_edit' => null, // по умолчанию значение show_ui
        'rewrite' => array('slug' => 'product-category'),
    );
    register_taxonomy('product-category', array('product'), $args);
}

add_action('init', 'register_product'); // Использовать функцию только внутри хука init

function register_product()
{
    $labels = array(
        'name' => 'Товары',
        'singular_name' => 'Товары', // админ панель Добавить->Функцию
        'add_new' => 'Добавить Товар',
        'add_new_item' => 'Добавить Товар для категории', // заголовок тега <title>
        'edit_item' => 'Редактировать Товар',
        'new_item' => 'Новый Товар',
        'all_items' => 'Все Товары',
        'view_item' => 'Просмотр Товара на сайте',
        'search_items' => 'Искать Товар',
        'not_found' => 'Товар не найдено.',
        'menu_name' => 'Товары' // ссылка в меню в админке
    );
    $args = array(
        'exclude_from_search' => true,
        'labels' => $labels,
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'menu_position' => 5,
        'show_in_nav_menus' => true,
        'publicly_queryable' => true,
        'has_archive' => true,
        'query_var' => true,
        'can_export' => true,
        'supports' => array('title', 'editor', 'excerpt', 'thumbnail', 'comments'),


        //'taxonomies' => array('post_tag')
    );
    register_post_type('product', $args);
}
add_action('init', 'register_service');

function register_service()
{
    $labels = array(
        'name' => 'Услуги',
        'singular_name' => 'Услуги', // админ панель Добавить->Функцию
        'add_new' => 'Добавить Услугу',
        'add_new_item' => 'Добавить Услугу для категории', // заголовок тега <title>
        'edit_item' => 'Редактировать Услугу',
        'new_item' => 'Новая Услуга',
        'all_items' => 'Все Услуги',
        'view_item' => 'Просмотр Услуги на сайте',
        'search_items' => 'Искать Услугу',
        'not_found' => 'Услугу не найдено.',
        'menu_name' => 'Услуги' // ссылка в меню в админке
    );
    $args = array(
        'exclude_from_search' => true,
        'labels' => $labels,
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'menu_position' => 5,
        'show_in_nav_menus' => true,
        'publicly_queryable' => true,
        'has_archive' => true,
        'query_var' => true,
        'can_export' => true,
        'supports' => array('title', 'editor', 'excerpt', 'thumbnail', 'comments'),


        //'taxonomies' => array('post_tag')
    );
    register_post_type('gemini_service', $args);
}

function gemini_image_sizes()
{

    add_image_size( 'custom-size', 220, 220, true );
//    //todo: add tablet size
//    add_image_size('slide-394', 394, 146, true);
}

add_filter('wp_calculate_image_sizes', 'gemini_image_sizes', 10, 5);


/*
 * Get srcset for img
 */

function get_srcset_by_img_src($image_src)
{
    global $wpdb;
    $query = "SELECT ID FROM {$wpdb->posts} WHERE guid='$image_src'";

    $id = $wpdb->get_var($query);

    return wp_get_attachment_image_srcset($id);
}

/*
 * Enable ajax
 */

add_action('wp_enqueue_scripts', 'myajax_data', 99);
function myajax_data()
{

    wp_localize_script('form_js', 'myajax',
        array(
            'url' => admin_url('admin-ajax.php'),
            'nonce' => wp_create_nonce('myajax-nonce')

        )
    );

}

/*
 * Rename mail(letter) Title
 */
function change_name($name)
{
    return 'Gemini';
}

add_filter('wp_mail_from_name', 'change_name');


/*
 * Ajax subscribe\form
 */

add_action('wp_ajax_my_contact_form', 'contact');
add_action('wp_ajax_nopriv_my_contact_form', 'contact');

function contact()
{
    check_ajax_referer('myajax-nonce', 'security');
    $message = 'Имя: ' . $_POST['name'];
    $message .= '   Почта: ' . $_POST['email'];
    $message .= '   Сообщение: ' . $_POST['msg'];

    $admin_email = get_option('admin_email');
    wp_mail($admin_email, 'Форма обратной связи', $message);

    wp_die();
}

add_action('wp_ajax_my_subscribe_contact_form', 'subscribe');
add_action('wp_ajax_nopriv_my_subscribe_contact_form', 'subscribe');

function subscribe()
{
    check_ajax_referer('myajax-nonce', 'security');
    $message = 'Телефон: ' . $_POST['phone'];


    $admin_email = get_option('admin_email');
    wp_mail($admin_email, 'Форма подписки', $message);

    wp_die();
}

/*
 * Add class for next\previous link
 */
function posts_link_next_class($format)
{
    $format = str_replace('href=', 'class="right-arrow" href=', $format);
    return $format;
}

add_filter('next_post_link', 'posts_link_next_class');

function posts_link_prev_class($format)
{
    $format = str_replace('href=', 'class="left-arrow" href=', $format);
    return $format;
}

add_filter('previous_post_link', 'posts_link_prev_class');

/*
 * Ajax category serach
 */
add_action('wp_ajax_my_search_category_form', 'category_search');
add_action('wp_ajax_nopriv_my_search_category_form', 'category_search');

function category_search()
{
    check_ajax_referer('myajax-nonce', 'security');
    $cat_id = $_POST['cat_id'];

    $the_query = new WP_Query(array('cat' => $cat_id));

    while ($the_query->have_posts()):
        $the_query->the_post();
        // get_template_part('template-parts/sidebar-post-content');
    endwhile;

    wp_reset_postdata();

    wp_die();
}

/*
 * Ajax serach
 */
add_action('wp_ajax_my_search_form', 'search');
add_action('wp_ajax_nopriv_my_search_form', 'search');

function search()
{

    check_ajax_referer('myajax-nonce', 'security');
    $cat_id = $_POST['cat_id'];
    $key = $_POST['key'];

    if ($cat_id != ''):
        $the_query = new WP_Query(array('s' => $key, 'cat' => $cat_id,'post_type' => 'post'));
    else:
        $the_query = new WP_Query(array('s' => $key,'post_type' => 'post'));
    endif;
    while ($the_query->have_posts()):
        $the_query->the_post();
        get_template_part('template-parts/sidebar-post-content');
    endwhile;

    wp_reset_postdata();

    wp_die();
}




// mobile function

$desktop = 'gemini.ua';
$mobile = 'm.gemini.ua';

add_action('wp_ajax_site_change', 'site_change');
add_action('wp_ajax_nopriv_site_change', 'site_change');

function site_change()
{
    session_start();
    check_ajax_referer('myajax-nonce', 'security');
    // $_SESSION['site_change'] = '1';
    setcookie("site_change_c", '1', time()+900, '/', 'm.gemini.ua');
    setcookie("site_change_c", '1', time()+900, '/', 'gemini.ua');

    wp_die();
}


 function check_site_version() {

     $desktop = 'gemini.ua';
     $mobile = 'm.gemini.ua';



     if ( !$_COOKIE['site_change_c'] ) {
         if ( is_mobile() && $_SERVER['HTTP_HOST'] == $desktop  ) {
               wp_redirect( 'https://' . $mobile . $_SERVER['REQUEST_URI'] ) ;
             exit();
         }

         if ( !( is_mobile() ) && $_SERVER['HTTP_HOST'] == $mobile) {
             wp_redirect('https://' .  $desktop . $_SERVER['REQUEST_URI'] );
             exit();
         }


         if ( $_SERVER['HTTP_HOST'] == $mobile ) {
             if (class_exists("MobileSmart"))
                 {
                   $mobile_smart = new MobileSmart();

                     add_filter('template', array(&$mobile_smart, 'filter_switchTheme'));
                     add_filter('stylesheet', array(&$mobile_smart, 'filter_switchTheme_stylesheet'));
                 }
         }
      }
 }



// setcookie("site_change_c", '100', time()+900, '/', '.test.redcarlos.com');
// unset($_COOKIE['site_change_c']);


// var_dump($_COOKIE["site_change_c"]);

session_start();

if (isset($_SESSION['LAST_ACTIVITY']) && (time() - $_SESSION['LAST_ACTIVITY'] > 100)) {
    // $_SESSION['site_change'] = '0';
    unset($_COOKIE['site_change_c']);
}

$_SESSION['LAST_ACTIVITY'] = time(); // update last activity time

 add_action('init', 'check_site_version');



