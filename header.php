<?php
/**
 * The template for displaying the header
 */
?><!DOCTYPE html>
<html lang="ua">
<head>
    <meta charset="UTF-8">
    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
    <meta http-equiv="X-UA-Compatible" content="ie=">
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=0">
    <meta property="og:image" content="path/to/image.jpg">
    <link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/assets/img/favicon/favicon.ico" type="image/x-icon">
    <link rel="apple-touch-icon" href="<?php echo get_template_directory_uri(); ?>/assets/img/favicon/apple-touch-icon.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo get_template_directory_uri(); ?>/assets/img/favicon/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="114x114" href="<?php echo get_template_directory_uri(); ?>/assets/img/favicon/apple-touch-icon-114x114.png">
    <!-- Chrome, Firefox OS and Opera -->
    <meta name="theme-color" content="#f0f0f0">
    <!-- Windows Phone -->
    <meta name="msapplication-navbutton-color" content="#f0f0f0">
    <!-- iOS Safari -->
    <meta name="apple-mobile-web-app-status-bar-style" content="#f0f0f0">
    <?php wp_head(); ?>

    <title><?php the_title(); ?></title>
    <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-89529702-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-‎89529702-1');
</script>

<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-NS37QGL');</script>
<!-- End Google Tag Manager -->
</head>
<body data-current-language="<?php echo apply_filters( 'wpml_current_language', NULL );  ?>">
    <div class="preloader">
    <div class="preloader-wrap">
        <i class="icon-logo"></i>
        <i class="icon-logo"></i>
        <i class="icon-logo"></i>
        <i class="icon-logo"></i>
        <i class="icon-logo"></i>
        <i class="icon-logo"></i>
    </div>
</div>
<style>
    .preloader{
        position: fixed;
        top: 0;
        left: 0;
        right: 0;
        bottom:0;
        background: #f0f0f0;
        z-index:1000;
    }
    .preloader-wrap {
        position: absolute;
        top: 50%;
        left: 50%;
        transform: translate(-50%, -50%) rotate(-180deg);
        width: 100px;
        height: 100px;
    }
    .preloader-wrap .icon-logo {
        position: absolute;
        width: 70px;
        height: 90px;
        opacity: 0;
        transform: rotate(225deg);
        animation-iteration-count: infinite;
        animation-name: orbit;
        animation-duration: 5.5s;
        color: #DB143C;
    }
    .preloader-wrap .icon-logo:nth-child(2) {
        animation-delay: 240ms;
    }
    .preloader-wrap .icon-logo:nth-child(3) {
        animation-delay: 480ms;
    }
    .preloader-wrap .icon-logo:nth-child(4) {
        animation-delay: 720ms;
    }
    .preloader-wrap .icon-logo:nth-child(5) {
        animation-delay: 960ms;
    }
    .preloader-wrap .icon-logo:nth-child(6) {
        animation-delay: 1200ms;
    }
    .preloader-wrap .icon-logo:nth-child(7) {
        animation-delay: 1440ms;
    }
    .preloader-wrap .icon-logo:nth-child(8) {
        animation-delay: 1680ms;
    }

    @keyframes orbit {
        0% {
            transform: rotate(225deg);
            opacity: 1;
            animation-timing-function: ease-out;
        }
        7% {
            transform: rotate(345deg);
            animation-timing-function: linear;
        }
        30% {
            transform: rotate(455deg);
            animation-timing-function: ease-in-out;
        }
        39% {
            transform: rotate(690deg);
            animation-timing-function: linear;
        }
        70% {
            transform: rotate(815deg);
            opacity: 1;
            animation-timing-function: ease-out;
        }
        75% {
            transform: rotate(945deg);
            animation-timing-function: ease-out;
        }
        76% {
            transform: rotate(945deg);
            opacity: 0;
        }
        100% {
            transform: rotate(945deg);
            opacity: 0;
        }
    }

</style>
<header class="main-header">
    <div class="container">
        <div class="header-wrap">
            <a href="<?php echo home_url(); ?>" class="logo">
                <?php
                $image_src = get_field('header_logo','option');
                $bg_srcset = get_srcset_by_img_src($image_src);
                ?>

                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/logo.svg" alt="image">
            </a>
            <!-- /.logo -->
            <nav>
                <?php
                /*
                 * Main menu
                 */
                $menuParameters = array(
                    'theme_location'     => 'main_menu',
                    'container'       => false,
                    'echo'            => true,
                    'items_wrap'        => '<ul class="menu">%3$s</ul>',
                    'depth'           => 0,
                );
                echo strip_tags(wp_nav_menu( $menuParameters ), '<a>' );
                ?>
            </nav>
            <div class="contacts-block">
                <?php $news = get_pages(array(
                    'meta_key' => '_wp_page_template',
                    'meta_value' => 'template/template-contact.php'
                    )); ?>
                <a class="icon-phone" href="tel:+380682395588"></a>
                <a class="icon-email" href="mailto:zakaz@caffe.com.ua"></a>
                <a class="icon-map" href="<?php echo get_post_permalink($news[0]->ID); ?>"></a>
                <div class="lang_select">
                    <?php do_action('wpml_add_language_selector'); ?>
                </div>
<!--                <div class="lang-select">-->
<!--                    <span class="current-lang"></span>-->
<!--                    <ul>-->
<!--                        <li></li>-->
<!--                    </ul>-->
<!--                </div>-->
                <!-- /.lang-select -->

            </div>
        </div>
        <!-- /.header-wrap -->
    </div>
    <!-- /.container -->
</header>

<div class="content-wrap">
