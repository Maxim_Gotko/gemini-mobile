<?php
/**
 * The template for displaying the News.
 *
 * This page template will display any functions hooked into the `homepage` action.
 * By default this includes a variety of product displays and the page content itself. To change the order or toggle these components
 * use the Homepage Control plugin.
 * https://wordpress.org/plugins/homepage-control/
 *
 * Template name: News
 *
 * @package faraday
 */

get_header();

?>
    <div class="header-news">
        <div class="container">
            <a href="javascript:history.go(-1); " class="left-arrow">Новини</a>
        </div>
    </div>
    <!-- CONTENT START -->
    <div class="container">
        <div class="news-sidebar">
            <form class="search_mobile">
                <div>
                    <input type="text"  class="search-input">
                    <input type="submit" id="searchsubmit" value="Search">
                </div>
                <label for="">
                    <?php echo __('Select category', 'gemini'); ?>
                </label>

                <?php
                /*
                 * Select all post category
                 */
                $terms = get_terms(array(
                    'taxonomy' => 'category',
                    'order' => 'DESC'
                ));
                ?>


                <select name="" class="search-select-input_mobile" id="">
                    <?php
                    /*
                     * Select all post category
                     */
                    $terms = get_terms( array(
                        'taxonomy' => 'category',
                        'order' => 'DESC'
                    ) );?>
                    <option value="none"></option>
                    <?php foreach($terms as $term): ?>
                        <option value="<?php echo $term->term_id;  ?>"><?php echo $term->name;  ?></option>
                    <?php endforeach; ?>
                </select>

            </form>
            <div class="news-list">


                <?php
                /*
                 * Select * posts
                 */
                $args = array(
                    'order' => 'DESC'
                );
                $the_query = new WP_Query($args);
                while ($the_query->have_posts()) :
                    $the_query->the_post();
                    get_template_part('template-parts/sidebar-post-content_mobile');
                endwhile; ?>


            </div>
            <a href="#" class="show-more">Показати ще &darr;</a>


        </div>
    </div>
<?php
get_footer();
