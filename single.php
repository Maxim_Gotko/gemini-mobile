<?php
/**
 * The template for displaying all single posts and attachments
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */

get_header(); 
$news = get_pages(array(
    'meta_key' => '_wp_page_template',
    'meta_value' => 'template-news.php'
));
?>

<div class="header-news">
    <div class="container">
<!--        <a href="--><?php //echo get_post_permalink($news[0]->ID); ?><!--">--><?php //echo __('News', 'gemini'); ?><!-- </a>-->
        <a href="<?php echo get_post_permalink(get_option( 'page_for_posts' )); ?>"><?php echo __('News', 'gemini'); ?> </a>
    </div>
    <!-- /.container -->
</div>
<!-- /.news-category -->
<div class="news-post-section">
    <div class="container no-padding-right">
        <div class="news-post-block">

            <?php
            // Start the loop.
            while (have_posts()) : the_post();

                get_template_part('template-parts/content');

            endwhile;
            ?>
            <!-- /.news-post -->
            </div>
            <!-- /.news-sidebar -->
        </div>
        <!-- /.news-post-block -->
    </div>
    <!-- /.container -->
</div>

<!-- /.news-post-section -->

<?php get_footer(); ?>
