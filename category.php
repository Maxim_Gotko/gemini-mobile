<?php
/**
 * The template for displaying Category pages
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header(); ?>
                    <!-- Blog Bar -->
                    <div class="col-sm-9 animate fadeInRight" data-wow-delay="0.4s">
                        <!--  Blog Posts -->
                        <div class="blog-posts medium-images">
                            <ul>
                                <?php while (have_posts()) : the_post(); ?>
                                    <!--  Posts 1 -->
                                    <li class="animate fadeInUp" data-wow-delay="0.4s">
                                        <div class="row">
                                            <!--  Image -->
                                            <div class="col-sm-5"><img class="img-responsive"
                                                                       src="<?php echo the_post_thumbnail_url(); ?>"
                                                                       alt=""></div>

                                            <!--  Post Details -->
                                            <div class="col-sm-7"><span
                                                        class="tags"><?php foreach ((get_the_category()) as $category) {
                                                        echo $category->cat_name . ' ';
                                                    } ?></span> <a
                                                        href="#."
                                                        class="tittle-post font-playfair"><?php the_title(); ?></a>
                                                <p><?php echo get_the_excerpt(); ?></p>
                                                <ul class="info">
                                                    <li><i class="fa fa-calendar-o"></i><?php echo get_the_date(); ?></li>
                                                </ul>
                                                <!--  Post Info -->
                                                <a class="btn btn-small btn-dark"
                                                   href="<?php echo get_permalink(); ?>">Детальніше</a>
                                            </div>
                                        </div>
                                    </li>
                                <?php endwhile; // end of the loop. ?>
                            </ul>
                        </div>

                    </div>
	<?php get_footer();?>