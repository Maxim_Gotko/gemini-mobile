<?php

get_header(); ?>
<?php
/*
 * Get current taxonomy
 */

$cur_term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );
$term_children = get_terms( array(
    'taxonomy' => 'product-category',
    'parent' => $cur_term->term_id,
    'orderby' => 'meta_value',
    'meta_key' => 'order'
) );

global $sitepress;
$current_language = $sitepress->get_current_language();
if(count($term_children) != 0): ?>
<div class="header-category">
    <div class="container">
        <h1> <a href="javascript:history.go(-1); " class="left-arrow"><?php echo $cur_term->name; ?></a></h1>
    </div>
    <!-- /.container -->
</div>
<!-- /.header-category -->
<div class="subcategories-section">
    <div class="container">
        <div class="subcategories-block">
           <?php
            /*
             * Get subcategories
             */
           foreach( $term_children as $child_term ):
            ?>
              <!-- /.subcategories-item -->
            <a href="<?php echo get_term_link( $child_term, 'product-category' ); ?>" class="subcategories-item">
                <div class="subcategories-img-wrap">
                    <?php $image_src = get_field('cat_img', $child_term ); ?>
                    <img src="<?php echo $image_src; ?>" srcset="<?php echo $image_src; ?> , <?php echo get_srcset_by_img_src($image_src); ?>" alt="image">
                </div>
                <!-- /.subcategories-img-wrap -->
                <h3><?php echo $child_term->name; ?></h3>
                <p>
                    <?php echo $child_term->description;  ?>
                </p>
            </a>
            <?php endforeach; ?>
            <!-- /.subcategories-item -->
        </div>
        <!-- /.subcategories-block -->
    </div>
    <!-- /.container -->
</div>
<!-- /.subcategories-section -->

<?php else: ?>

    <div class="header-subcategory">
        <div class="container">
            <h1><a href="javascript:history.go(-1); "><?php echo $cur_term->name; ?></a></h1>

        </div>
        <!-- /.container -->
    </div>
    <!-- /.header-subcategory -->
    <div class="subcategories-product-section">
        <div class="container">
            <div class="products-block">

                <?php
                /*
                 * Select all products from current term
                 */
                $args = array('post_type' => 'product',
                    'order'   => 'DESC',
                    'tax_query' => array(
                        array(
                            'taxonomy' => 'product-category',
                            'field' => 'name',
                            'terms'     => $cur_term->name
                        ),
                    ),
                );
                $the_query = new WP_Query($args);
                while ( $the_query->have_posts()) :
                $the_query->the_post(); ?>
                <a href="<?php the_permalink(); ?>" class="product-item">
                    <div class="product-item-img">
                    <?php the_post_thumbnail(); ?>
                    </div>
                    <!-- /.product-item-img -->
                    <p><?php the_title(); ?></p>
                </a>
                <!-- /.product-item -->
                <?php endwhile; ?>


            </div>
            <!-- /.subcategories-product-block -->
        </div>
        <!-- /.container -->
    </div>
    <!-- /.subcategories-product-section -->

<?php endif; ?>


<?php get_footer(); ?>
