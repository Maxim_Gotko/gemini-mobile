<?php

get_header(); $page_id = get_the_ID(); ?>

<div class="header-tasting">
  
    <div class="container">
        <h1><a href="javascript:history.go(-1); " class="left-arrow"><?php the_title(); ?></a></h1>
      
    </div>
    <!-- /.container -->
</div>
<!-- /.news-category -->
<div class="container">
    <?php while (have_posts()) : the_post(); ?>
        <div class="tasting-info">
            <?php $gallery = get_field('service-gallery', $page_id ); 

                if ( $gallery[0]['img'] ) {
      
            ?>
            <div class="gallery-block">
                <div class="swiper-container gallery-top">
                    <div class="swiper-wrapper">
                        <?php while ( have_rows('service-gallery', $page_id ) ) : the_row(); ?>
                            <div class="swiper-slide" style="background-image:url(<?php the_sub_field('img'); ?>)"></div>
                        <?php endwhile; ?>

                    </div>
                    <!-- Add Arrows -->
                    <div class="gallery-button-next"><span></span></div>
                    <div class="gallery-button-prev"><span></span></div>
                </div>
                <div class="swiper-container gallery-thumbs">
                    <div class="swiper-wrapper">
                        <?php while ( have_rows('service-gallery', $page_id ) ) : the_row(); ?>
                            <div class="swiper-slide" style="background-image:url(<?php the_sub_field('img'); ?>)"></div>
                        <?php endwhile; ?>
                    </div>
                </div>
            </div>
            <?php } ?>
            <?php the_content(); ?>
        </div>
    <?php endwhile; ?>
    <!-- /.html -->
</div>
<!-- /.container -->


<!-- /.services-section -->



<!-- /.callback-section -->

<!-- /.callback-section -->

<!-- /.callback-section -->
<?php get_footer(); ?>
