<?php
/**
 * The template part for displaying sidebar-post-content
 */
?>  <a class="news-list-item" href="<?php the_permalink(); ?>">
    <span class="img-wrap">
            <img src="<?php echo get_the_post_thumbnail_url(get_the_ID()); ?>" alt="image">
    </span>
    <!-- /.img-wrap -->

    <h3><?php the_title(); ?></h3>
    <span class="post-date">
        <?php echo get_the_date(); ?>
    </span>
    <p><?php  $content = get_the_content();
                            echo wp_trim_words( $content , 20, '...' );

                             ?></p>
</a>