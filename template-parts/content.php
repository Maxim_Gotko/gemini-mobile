<?php
/**
 * The template part for displaying content
 */
?><div class="news-post">


    <h1><?php the_title(); ?></h1>
                <span class="post-date">
                <?php echo get_the_date(); ?>
            </span>
    <!-- /.post-date -->
    <?php $gallery = get_field('gallery', $page_id ); if( count($gallery[0]) != 0): ?>
    <div class="gallery-block">
        <div class="swiper-container gallery-top">
            <div class="swiper-wrapper">
                <?php while ( have_rows('gallery', $page_id ) ) : the_row(); ?>
                    <div class="swiper-slide" style="background-image:url(<?php the_sub_field('img'); ?>)"></div>
                <?php endwhile; ?>

            </div>
            <!-- Add Arrows -->
            <div class="gallery-button-next"><span></span></div>
            <div class="gallery-button-prev"><span></span></div>
        </div>
        <div class="swiper-container gallery-thumbs">
            <div class="swiper-wrapper">
                <?php while ( have_rows('gallery', $page_id ) ) : the_row(); ?>
                    <div class="swiper-slide" style="background-image:url(<?php the_sub_field('img'); ?>)"></div>
                <?php endwhile; ?>
            </div>
        </div>
    </div>
    <?php else: ?>
        <div class="news-video">
            <?php the_field('video', $page_id); ?>
        </div>
    <?php endif; ?>
    <!-- /.gallery-block -->

    <div class="text-block">
        <?php the_content(); ?>
    </div>
</div>