<?php
/**
 * The template part for displaying callback-section content
 */
?>
<div class="callback-section">


    <div class="callback-block">
        <div class="map" id="map"></div>
        <script>
            var img_src = '<?php echo get_template_directory_uri(); ?>/assets/img/map_icon.png'
        </script>
        <!-- /.map -->
        <!--            --><?php //while ( have_rows('dealers', 'option') ) : the_row(); ?>
        <!---->
        <!--                <div class="contact-info">-->
        <!--                    <ul>-->
        <!--                        <li class="icon-map">-->
        <!--                            <strong>--><?php //the_sub_field('title_adress'); ?><!--</strong>-->
        <!--                            <p>--><?php //the_sub_field('adress'); ?><!--</p>-->
        <!--                        </li>-->
        <!--                        <li class="icon-phone">-->
        <!--                            <strong>--><?php //the_sub_field('title_phone'); ?><!--</strong>-->
        <!--                            --><?php //the_sub_field('phones'); ?>
        <!--                        </li>-->
        <!--                        <li class="icon-email">-->
        <!--                            <strong>--><?php //the_sub_field('title_mail'); ?><!--</strong>-->
        <!--                            <p>--><?php //the_sub_field('mail'); ?><!--</p>-->
        <!--                        </li>-->
        <!--                        <li class="icon-time">-->
        <!--                            <strong>--><?php //the_sub_field('title_work_time'); ?><!--</strong>-->
        <!--                            <p>--><?php //the_sub_field('work_time'); ?><!--</p>-->
        <!--                        </li>-->
        <!--                    </ul>-->
        <!--                </div>-->
        <!---->
        <!--            --><?php //endwhile; ?>
        <!-- /.dealer-item -->
        <?php while ( have_rows('contact_form', 'option') ) : the_row(); ?>
            <div class="contact-block">

                <?php while (have_rows('contact_info_1', $page_id)) : the_row(); ?>

                    <div class="contact-info">
                        <ul>
                            <li>
                                <strong><?php the_sub_field('title_adress'); ?></strong>

                                <p><?php the_sub_field('adress'); ?></p>
                            </li>
                            <!--                                <li class="icon-phone">-->
                            <!--                                    <strong>--><?php //the_sub_field('title_phone'); ?><!--</strong>-->
                            <!--                                    --><?php //the_sub_field('phones'); ?>
                            <!--                                </li>-->
                            <!--                                <li class="icon-email">-->
                            <!--                                    <strong>--><?php //the_sub_field('title_mail'); ?><!--</strong>-->
                            <!---->
                            <!--                                    <p>--><?php //the_sub_field('mail'); ?><!--</p>-->
                            <!--                                </li>-->
                            <li>
                                <span><?php the_sub_field('title_work_time'); ?></span>

                                <p><?php the_sub_field('work_time'); ?></p>
                            </li>
                        </ul>
                    </div>

                <?php endwhile; ?>
                <?php while (have_rows('contact_info_2', $page_id)) : the_row(); ?>

                    <div class="contact-info">
                        <ul>
                            <li>
                                <strong><?php the_sub_field('title_adress'); ?></strong>

                                <p><?php the_sub_field('adress'); ?></p>
                            </li>
                            <!--                                <li class="icon-phone">-->
                            <!--                                    <strong>--><?php //the_sub_field('title_phone'); ?><!--</strong>-->
                            <!--                                    --><?php //the_sub_field('phones'); ?>
                            <!--                                </li>-->
                            <!--                                <li class="icon-email">-->
                            <!--                                    <strong>--><?php //the_sub_field('title_mail'); ?><!--</strong>-->
                            <!---->
                            <!--                                    <p>--><?php //the_sub_field('mail'); ?><!--</p>-->
                            <!--                                </li>-->
                            <li>
                                <span><?php the_sub_field('title_work_time'); ?></span>

                                <p><?php the_sub_field('work_time'); ?></p>
                            </li>
                        </ul>
                    </div>

                <?php endwhile; ?>
                <?php while (have_rows('contact_info_3', $page_id)) : the_row(); ?>

                    <div class="contact-info">
                        <ul>
                            <li>
                                <strong><?php the_sub_field('title_adress'); ?></strong>

                                <p><?php the_sub_field('adress'); ?></p>
                            </li>
                            <!--                                <li class="icon-phone">-->
                            <!--                                    <strong>--><?php //the_sub_field('title_phone'); ?><!--</strong>-->
                            <!--                                    --><?php //the_sub_field('phones'); ?>
                            <!--                                </li>-->
                            <!--                                <li class="icon-email">-->
                            <!--                                    <strong>--><?php //the_sub_field('title_mail'); ?><!--</strong>-->
                            <!---->
                            <!--                                    <p>--><?php //the_sub_field('mail'); ?><!--</p>-->
                            <!--                                </li>-->
                            <li>
                                <span><?php the_sub_field('title_work_time'); ?></span>

                                <p><?php the_sub_field('work_time'); ?></p>
                            </li>
                        </ul>
                    </div>

                <?php endwhile; ?>
            </div>
        <?php endwhile; ?>
        <!-- /.calback-form -->
    </div>
    <!-- /.callback-block -->

</div>
<!-- /.callback-section -->
