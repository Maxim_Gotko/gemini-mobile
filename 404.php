<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */

get_header(); ?>

<div class="section-404">
    <div class="parallax-scene" id="scene">
        <div class="header-bg-layer-1" data-depth="0.2"></div>
        <!-- /.header-bg-layer-1 -->
        <div class="header-bg-layer-2" data-depth="0.1"></div>
        <!-- /.header-bg-layer-2 -->
        <div class="header-bg-layer-3" data-depth="0.3"></div>
        <!-- /.header-bg-layer-2 -->
    </div>
    <div class="container">
        <h1>Кава закінчилась :(</h1>
        <div class="main-404">
            <span>4</span>
            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/404.png" alt="0">
            <span>4</span>
        </div>
        <!-- /.main404 -->
        <div class="info-404">
            <strong>Що тепер робити?</strong>
            <ul>
                <li>1. Повернутись на <a href="#" class="to-main">головну сторінку</a></li>
                <li>2. Написати нам все, що ви про нас думаєте на скриньку <a href="mailto:info@gemini.ua">info@gemini.ua</a></li>
                <li>3. Продовжуте дивитись в монітор та через 30 секунд вас
                    буде автоматично переадресовано на головну сторінку.</li>
            </ul>
        </div>
        <!-- /.info-404 -->
    </div>
    <!-- /.container -->
</div>
<script type="text/javascript">
    setTimeout(function() { window.location.href = '//m.gemini.ua'; }, 30000);
</script>


<?php get_footer(); ?>
