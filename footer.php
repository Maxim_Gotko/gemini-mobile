<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the "site-content" div and all content after.
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */
?>
</div>
<footer>
    <div class="container">
        <div class="footer-wrap">

            <div class="copyright">
                <?php the_field('copyright', 'option'); ?>
            </div>
            <!-- /.copyright -->

            <div id="site_switcher" class="site_switcher">Полная версия сайта</div>

            <div class="social-link">
                <a href="<?php the_field('facebook', 'option'); ?>" class="icon-fb"></a>
                <a href="<?php the_field('instagram', 'option'); ?>" class="icon-instagram"></a>
                <a href="<?php the_field('twitter', 'option'); ?>" class="icon-twitter"></a>
                <a href="<?php the_field('youtube', 'option'); ?>" class="icon-youtube-symbol"></a>
                <a href="<?php the_field('pinterest', 'option'); ?>" class="icon-pinterest"></a>
                <a href="<?php the_field('google', 'option'); ?>" class="icon-google-plus"></a>
                <a href="<?php the_field('viber', 'option'); ?>" class="icon-viber" rel="nofollow"></a>
                <a href="<?php the_field('telegram', 'option'); ?>" class="icon-telegram" rel="nofollow"></a>
                <a class="email"
                   href="mailto:<?php the_field('email', 'option'); ?>"><?php the_field('email', 'option'); ?></a>
                <!-- /.email -->
            </div>
            <!-- /.social-link -->
        </div>
        <!-- /.footer-wrap -->

    </div>
    <!-- /.container -->
    <a href="https://red-carlos.com" target="_blank" class="rc">
        by
        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/rc.svg" alt="RedCarlosStudio">
        <style>
            .rc {
                display: block;
                position: absolute;
                bottom: 3px;
                right: 3px;
                width: 130px;
                font-size: 11px;
                color: #4d4d4d;
            }

            .rc img {
                width: 80%;
                vertical-align: middle;
            }
        </style>
    </a>
</footer>



<?php wp_footer(); ?>

<script defer
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB5BEFeffM4Rw-Zjz0s-D_BAxbtnIyaU8E&callback">
</script>

<script>
    $(document).ready(function () {
        var marker;
        var map;
        function initMap() {
            var styleMap = [
                {
                    "featureType": "water",
                    "elementType": "geometry",
                    "stylers": [
                        {
                            "color": "#e9e9e9"
                        },
                        {
                            "lightness": 17
                        }
                    ]
                },
                {
                    "featureType": "landscape",
                    "elementType": "geometry",
                    "stylers": [
                        {
                            "color": "#f5f5f5"
                        },
                        {
                            "lightness": 20
                        }
                    ]
                },
                {
                    "featureType": "road.highway",
                    "elementType": "geometry.fill",
                    "stylers": [
                        {
                            "color": "#ffffff"
                        },
                        {
                            "lightness": 17
                        }
                    ]
                },
                {
                    "featureType": "road.highway",
                    "elementType": "geometry.stroke",
                    "stylers": [
                        {
                            "color": "#ffffff"
                        },
                        {
                            "lightness": 29
                        },
                        {
                            "weight": 0.2
                        }
                    ]
                },
                {
                    "featureType": "road.arterial",
                    "elementType": "geometry",
                    "stylers": [
                        {
                            "color": "#ffffff"
                        },
                        {
                            "lightness": 18
                        }
                    ]
                },
                {
                    "featureType": "road.local",
                    "elementType": "geometry",
                    "stylers": [
                        {
                            "color": "#ffffff"
                        },
                        {
                            "lightness": 16
                        }
                    ]
                },
                {
                    "featureType": "poi",
                    "elementType": "geometry",
                    "stylers": [
                        {
                            "color": "#f5f5f5"
                        },
                        {
                            "lightness": 21
                        }
                    ]
                },
                {
                    "featureType": "poi.park",
                    "elementType": "geometry",
                    "stylers": [
                        {
                            "color": "#dedede"
                        },
                        {
                            "lightness": 21
                        }
                    ]
                },
                {
                    "elementType": "labels.text.stroke",
                    "stylers": [
                        {
                            "visibility": "on"
                        },
                        {
                            "color": "#ffffff"
                        },
                        {
                            "lightness": 16
                        }
                    ]
                },
                {
                    "elementType": "labels.text.fill",
                    "stylers": [
                        {
                            "saturation": 36
                        },
                        {
                            "color": "#333333"
                        },
                        {
                            "lightness": 40
                        }
                    ]
                },
                {
                    "elementType": "labels.icon",
                    "stylers": [
                        {
                            "visibility": "off"
                        }
                    ]
                },
                {
                    "featureType": "transit",
                    "elementType": "geometry",
                    "stylers": [
                        {
                            "color": "#f2f2f2"
                        },
                        {
                            "lightness": 19
                        }
                    ]
                },
                {
                    "featureType": "administrative",
                    "elementType": "geometry.fill",
                    "stylers": [
                        {
                            "color": "#fefefe"
                        },
                        {
                            "lightness": 20
                        }
                    ]
                },
                {
                    "featureType": "administrative",
                    "elementType": "geometry.stroke",
                    "stylers": [
                        {
                            "color": "#fefefe"
                        },
                        {
                            "lightness": 17
                        },
                        {
                            "weight": 1.2
                        }
                    ]
                }
            ];

            map = new google.maps.Map(document.getElementById('map'), {
                zoom: 12,
                center: {lat: 50.450309 - 0.03, lng: 30.523766 },
                styles: styleMap,
                mapTypeControl:false
            });
            var imageUrl = img_src;

            var image = {
                url:imageUrl,
                size: new google.maps.Size(100, 100),
                origin: new google.maps.Point(0, 0),
                anchor: new google.maps.Point(25, 50)
            };
            marker = new google.maps.Marker({
                position: {lat: 50.450309, lng: 30.523766},
                map: map,
                icon: image
            });

        }
        initMap();

    });
</script>
<script>
    $(window).on('load', function () {
        var $preloader = $('.preloader'),
            $spinner = $preloader.find('.preloader-wrap');
        $spinner.fadeOut();
        $preloader.delay(250).fadeOut('slow');
    });
</script>
</body>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NS37QGL"
                  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
</html>
